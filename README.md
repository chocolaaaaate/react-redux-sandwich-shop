## Sample React & Redux project
This is a simple mock sandwich shop created using React with Redux for state management. The project uses TypeScript, and is unit tested. 

By Sumedh Kanade
