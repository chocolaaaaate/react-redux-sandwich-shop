import { Action } from "./shop";
import IFoodItem from "../model/FoodItem";
import Sandwich from '../model/Sandwich';
import { COMPLETE_ORDER_INTENTION } from './order';

// Action types 

export const ADD_INGREDIENT_INTENTION = "ADD_INGREDIENT_INTENTION";
export const REMOVE_INGREDIENT_INTENTION = "REMOVE_INGREDIENT_INTENTION";


// Action creators

export const addIngredientAction = (ingredient: IFoodItem): Action => {
    return {
        type: ADD_INGREDIENT_INTENTION,
        payload: {
            ingredient: ingredient
        }
    }
}

export const removeIngredientAction = (ingredient: IFoodItem): Action => {
    return {
        type: REMOVE_INGREDIENT_INTENTION,
        payload: {
            ingredient: ingredient
        }
    }
}


// Reducers

export const currentSandwichReducer = (state = new Sandwich(), action: Action) => {

    if (action.type === ADD_INGREDIENT_INTENTION) {
        const currentSandwich = { ...state };
        currentSandwich.ingredients =
            currentSandwich.ingredients || [];
        currentSandwich.ingredients.push(action.payload.ingredient)
        return currentSandwich;
    }
    else if (action.type === REMOVE_INGREDIENT_INTENTION) {
        const currentSandwich = { ...state };
        currentSandwich.ingredients = currentSandwich.ingredients.filter(ingr => ingr.name !== action.payload.ingredient.name);
        return currentSandwich;
    }
    else if (action.type === COMPLETE_ORDER_INTENTION) {
        return new Sandwich();
    }
    return state;
}



