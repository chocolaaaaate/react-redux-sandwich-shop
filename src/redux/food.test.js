import * as food from '../redux/food.ts';
import * as order from '../redux/order.ts';
import Sandwich from '../model/Sandwich';

const tomato = {
    name: "Tomato",
    cost: 2.5,
    quantity: 5
}

test('add ingredient action creator', () => {
    expect(food.addIngredientAction(tomato).type).toBe(food.ADD_INGREDIENT_INTENTION);
    expect(food.addIngredientAction(tomato).payload.ingredient).toBe(tomato);
});


test('remove ingredient action creator', () => {
    expect(food.removeIngredientAction(tomato).type).toBe(food.REMOVE_INGREDIENT_INTENTION);
    expect(food.removeIngredientAction(tomato).payload.ingredient).toBe(tomato);
});

test('current sandwich reducer - undefined initial', () => {
    expect(food.currentSandwichReducer(undefined, {})).toBeInstanceOf(Sandwich);
});

test('current sandwich reducer - add ingredients', () => {
    const sandwich = new Sandwich();
    expect(sandwich.ingredients).toBeUndefined();
    const sandwichPostReduction = food.currentSandwichReducer(
        sandwich,
        {
            type: food.ADD_INGREDIENT_INTENTION,
            payload: {
                ingredient: tomato
            }
        }
    );
    expect(sandwich).not.toBe(sandwichPostReduction);
    expect(sandwichPostReduction.ingredients.length).toBe(1);
    expect(sandwichPostReduction.ingredients[0]).toBe(tomato);

    const portobello = {
        name: "Portobello",
        cost: 3,
        quantity: 15
    }

    const sandwichPostPostReduction = food.currentSandwichReducer(
        sandwichPostReduction,
        {
            type: food.ADD_INGREDIENT_INTENTION,
            payload: {
                ingredient: portobello
            }
        }
    );
    expect(sandwichPostReduction).not.toBe(sandwichPostPostReduction);
    expect(sandwichPostPostReduction.ingredients.length).toBe(2);
    expect(sandwichPostPostReduction.ingredients[0]).toBe(tomato);
    expect(sandwichPostPostReduction.ingredients[1]).toBe(portobello);
});

test('current sandwich reducer - remove ingredients', () => {
    const sandwich1 = new Sandwich();

    const portobello = {
        name: "Portobello",
        cost: 3,
        quantity: 15
    }

    const sandwich2 = food.currentSandwichReducer(
        sandwich1,
        {
            type: food.ADD_INGREDIENT_INTENTION,
            payload: {
                ingredient: tomato
            }
        }
    );
    const sandwich3 = food.currentSandwichReducer(
        sandwich2,
        {
            type: food.ADD_INGREDIENT_INTENTION,
            payload: {
                ingredient: portobello
            }
        }
    );
    expect(sandwich3.ingredients.length).toBe(2);
    expect(sandwich3.ingredients[0]).toBe(tomato);
    expect(sandwich3.ingredients[1]).toBe(portobello);

    const sandwich4 = food.currentSandwichReducer(
        sandwich3,
        {
            type: food.REMOVE_INGREDIENT_INTENTION,
            payload: {
                ingredient: tomato
            }
        }
    );
    expect(sandwich4.ingredients.length).toBe(1);
    expect(sandwich4.ingredients[0]).toBe(portobello);

    const sandwich5 = food.currentSandwichReducer(
        sandwich4,
        {
            type: food.REMOVE_INGREDIENT_INTENTION,
            payload: {
                ingredient: portobello
            }
        }
    );
    expect(sandwich5.ingredients.length).toBe(0);
});

test('current sandwich reducer - new order initializes new sandwich', () => {
    const sandwich1 = new Sandwich();
    const sandwich2 = food.currentSandwichReducer(
        sandwich1,
        {
            type: food.ADD_INGREDIENT_INTENTION,
            payload: {
                ingredient: tomato
            }
        }
    );

    const newOrderSandwich = food.currentSandwichReducer(
        sandwich2,
        {
            type: order.COMPLETE_ORDER_INTENTION
        }
    );
    expect(newOrderSandwich.ingredients).toBeUndefined();
});
