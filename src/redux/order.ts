import FoodItem from "../model/FoodItem";
import { Action } from "./shop";
import Sandwich from "../model/Sandwich";
import Order from "../model/Order";
import { ADD_INGREDIENT_INTENTION, REMOVE_INGREDIENT_INTENTION } from './food';

export const COMPLETE_ORDER_INTENTION = "COMPLETE_ORDER_INTENTION";

export const completeOrderAction = (currentSandwich: Sandwich): Action => {
    return {
        type: COMPLETE_ORDER_INTENTION,
        payload: {
            currentSandwich: currentSandwich
        }
    }
}

/* REDUCERS */

export const completedOrdersReducer = (state = new Array<Order>(), action: Action) => {

    if (action.type === COMPLETE_ORDER_INTENTION) {
        const orders = [...state];
        orders.push({
            sandwich: action.payload.currentSandwich,
            total: action.payload
                .currentSandwich
                .ingredients.map(i => i.cost)
                .reduce((prev, curr) => prev + curr),
            timePlaced: new Date()
        });
        return orders;
    }
    return state;
}

export const currentOrderTotalReducer = (state = 0, action: Action) => {

    if (action.type === ADD_INGREDIENT_INTENTION) {
        const orderCost = state as number;
        return orderCost + (action.payload.ingredient as FoodItem).cost;
    }
    if (action.type === REMOVE_INGREDIENT_INTENTION) {
        const orderCost = state as number;
        return orderCost - (action.payload.ingredient as FoodItem).cost;
    }
    if (action.type === COMPLETE_ORDER_INTENTION) {
        return 0;
    }
    return state;
}

