import Sandwich from "../model/Sandwich";
import FoodItem from "../model/FoodItem";
import { createStore, combineReducers, compose } from 'redux';
import Order from "../model/Order";
import { currentSandwichReducer } from "./food";
import { completedOrdersReducer, currentOrderTotalReducer } from "./order";
import {stockReducer} from './stock';


export interface Action {
    type: string;
    payload?: any;
}

export interface IReduxState {
    currentSandwich: Sandwich;
    completedOrders: Order[];
    currentOrderTotal: number;
    stock: FoodItem[];
}

/* Combine reducers */

const combinedReducers = combineReducers({
    currentSandwich: currentSandwichReducer,
    completedOrders: completedOrdersReducer,
    currentOrderTotal: currentOrderTotalReducer,
    stock: stockReducer
});


const enhancers = compose(
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ ? (window as any).__REDUX_DEVTOOLS_EXTENSION__() : f => f
);

export const store = createStore(combinedReducers, enhancers);


