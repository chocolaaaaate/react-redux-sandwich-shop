import * as food from '../redux/food.ts';
import * as order from '../redux/order.ts';
import Sandwich from '../model/Sandwich';

const tomato = {
    name: "Tomato",
    cost: 2.5,
    quantity: 5
}

const olives = {
    name: "Olives",
    cost: 1.0,
    quantity: 2
}

const sandwich = {
    ingredients: [tomato, olives]
}

test('completeOrder action creator', () => {
    expect(order.completeOrderAction(sandwich).type).toBe(order.COMPLETE_ORDER_INTENTION);
    expect(order.completeOrderAction(sandwich).payload.currentSandwich).toBe(sandwich);
});

test('completedOrders reducer - initial', () => {
    const orders = order.completedOrdersReducer(undefined, { type: "nonexistent" });
    expect(orders.length).toBe(0);
});

test('completedOrders reducer - calculate order total', () => {
    const action = order.completeOrderAction(sandwich);
    const orders = order.completedOrdersReducer(undefined, action);
    expect(orders.length).toBe(1);

    const ord = orders[0];
    expect(ord.sandwich).toBe(sandwich);
    expect(ord.total).toBe(3.5);
    expect(ord.timePlaced).toBeInstanceOf(Date);
});

test('currentOrderTotal reducer - initial', () => {
    const initialTotal = order.currentOrderTotalReducer(undefined, { type: "nonexistent" });
    expect(initialTotal).toBe(0);
});

test('currentOrderTotal reducer - adding ingredients', () => {
    const initialTotal = 2;

    let newTotal = order.currentOrderTotalReducer(initialTotal, food.addIngredientAction(olives));
    expect(newTotal).toBe(3.0);

    newTotal = order.currentOrderTotalReducer(newTotal, food.addIngredientAction(tomato));
    expect(newTotal).toBe(5.5);
});

test('currentOrderTotal reducer - removing ingredients', () => {
    const initialTotal = 5.5;

    let newTotal = order.currentOrderTotalReducer(initialTotal, food.removeIngredientAction(olives));
    expect(newTotal).toBe(4.5);

    newTotal = order.currentOrderTotalReducer(newTotal, food.removeIngredientAction(tomato));
    expect(newTotal).toBe(2.0);
});

test('currentOrderTotal reducer - completing order', () => {
    const initialTotal = 5.5;

    let newTotal = order.currentOrderTotalReducer(initialTotal,
        {
            type: order.COMPLETE_ORDER_INTENTION
        }
    );
    expect(newTotal).toBe(0);
});
