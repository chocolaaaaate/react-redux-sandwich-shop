import { stock } from '../data/stock';
import { Action } from "./shop";
import { ADD_INGREDIENT_INTENTION, REMOVE_INGREDIENT_INTENTION } from './food';


export const stockReducer = (state = stock, action: Action) => {

    if (action.type === ADD_INGREDIENT_INTENTION) {
        const clonedStock = [...state];
        const ingredientIndexInStock = clonedStock.findIndex(item => item.name === action.payload.ingredient.name);
        if(clonedStock[ingredientIndexInStock].quantity === 0) {
            throw new Error("Stock would be negative if this action were to be allowed: " + JSON.stringify(action))
        }
        clonedStock[ingredientIndexInStock].quantity--;
        return clonedStock;
    }
    if (action.type === REMOVE_INGREDIENT_INTENTION) {
        const clonedStock = [...state];
        const ingredientIndexInStock = clonedStock.findIndex(item => item.name === action.payload.ingredient.name);
        clonedStock[ingredientIndexInStock].quantity++;
        return clonedStock;
    }
    return state;
}
