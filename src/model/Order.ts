import ISandwich from "./Sandwich";

export default interface IOrder {
    sandwich: ISandwich;
    total: number;
    timePlaced: Date;
}
