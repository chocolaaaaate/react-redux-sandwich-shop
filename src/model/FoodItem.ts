export default interface IFoodItem {
    name: string;
    cost: number;
    quantity: number;
}

