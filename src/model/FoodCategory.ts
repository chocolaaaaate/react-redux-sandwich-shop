import IFoodItem from "./FoodItem";

export default interface IFoodCategory {
    name: string;
    foodItems: IFoodItem[];
}

