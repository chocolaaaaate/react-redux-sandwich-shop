import IFoodItem from "../model/FoodItem";


/* MEATS */

export const chicken: IFoodItem = {
    name: "Chicken",
    cost: 5.0,
    quantity: 10
}

export const turkey: IFoodItem = {
    name: "Turkey",
    cost: 3.5,
    quantity: 10
}

export const tofu: IFoodItem = {
    name: "Tofu",
    cost: 3,
    quantity: 15
}


/* SAUCES */

export const mayo: IFoodItem = {
    name: "Mayo",
    cost: 0.1,
    quantity: 20
}

export const mustard: IFoodItem = {
    name: "Mustard",
    cost: 0.1,
    quantity: 20
}

export const ketchup: IFoodItem = {
    name: "Ketchup",
    cost: 0.1,
    quantity: 20
}

export const sriracha: IFoodItem = {
    name: "Sriracha",
    cost: 0.2,
    quantity: 15
}

/* CHEESES */

export const swiss: IFoodItem = {
    name: "Swiss",
    cost: 0.1,
    quantity: 1
}

export const provolone: IFoodItem = {
    name: "Provolone",
    cost: 0.1,
    quantity: 1
}

export const munster: IFoodItem = {
    name: "Munster",
    cost: 0.1,
    quantity: 1
}

/* VEGGIES */

export const lettuce: IFoodItem = {
    name: "Lettuce",
    cost: 0.05,
    quantity: 20
}

export const tomato: IFoodItem = {
    name: "Tomato",
    cost: 0.05,
    quantity: 20
}

export const olives: IFoodItem = {
    name: "Olives",
    cost: 0.1,
    quantity: 20
}

export const jalapeno: IFoodItem = {
    name: "Jalapeno",
    cost: 0.05,
    quantity: 20
}

export const meats: IFoodItem[] = [chicken, turkey, tofu];
export const cheeses: IFoodItem[] = [swiss, provolone, munster];
export const veggies: IFoodItem[] = [lettuce, tomato, olives, jalapeno];
export const sauces: IFoodItem[] = [mayo, mustard, ketchup, sriracha];

export const stock: IFoodItem[] = [...meats, ...cheeses, ...veggies, ...sauces];
