import React from 'react';
// import './App.css';
import { sauces, meats, veggies, cheeses } from './data/stock';
import { ContainedFoodGroup } from './components/FoodGroup';
import { ContainedManagerDashboard } from './components/ManagerDashboard';
import { ContainedOrder } from './components/Order';
import { Header } from './components/Header';


function App() {
  return (
    <div >
      <Header />
      <div className="container">
        <div className="row" style={{ paddingTop: "1rem" }}>
          <div className="col-sm-8 col-md-8 col-xs-12">
            <ContainedFoodGroup
              name="Meat"
              items={meats} />
            <ContainedFoodGroup
              name="Veggies"
              items={veggies} />
            <ContainedFoodGroup
              name="Cheese"
              items={cheeses} />
            <ContainedFoodGroup
              name="Sauce"
              items={sauces} />
          </div>
          <div className="col-sm-4 col-md-4 col-xs-12" style={{
            borderLeft: "1px solid lightgray"
          }}>
            <ContainedOrder />
          </div>
        </div>
        <div className="row">
          <div className="col" style={{ paddingTop: "1rem" }}>
            <ContainedManagerDashboard />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
