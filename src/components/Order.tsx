import * as React from 'react';
import { connect } from 'react-redux';
import { IReduxState } from "../redux/shop";
import Sandwich from '../model/Sandwich';
import { completeOrderAction } from '../redux/order';

export const Order = (props: {
    currentSandwich: Sandwich,
    currentOrderTotal: number,
    placeOrder: (order: Sandwich) => any
}) => {
    if (!props.currentSandwich) {
        return (<span></span>);
    }
    return (
        <div className="container">
            <div className="row">
                <div className="col">
                    <h4 style={{
                        fontVariant: "petite-caps",
                        color: "gray"
                    }}>your sandwich</h4>
                </div>
            </div>
            {!props.currentSandwich.ingredients && (<p>Please select your ingredients.</p>)}
            {props.currentSandwich.ingredients && (
                <React.Fragment>
                    <div className="row">
                        <div className="col">
                            <ul className="list-group">
                                {props.currentSandwich.ingredients?.map(
                                    itm =>
                                        <li
                                            className="list-group-item d-flex justify-content-between align-items-center"
                                            key={itm.name + "-" + Math.random()}>
                                            {itm.name}
                                            <span className="badge badge-light">${itm.cost.toFixed(2)}</span>
                                        </li>
                                )}
                                {props.currentSandwich.ingredients.length > 0 && <li className="list-group-item d-flex justify-content-between align-items-center">
                                    <b>Total:</b>
                                    <span >
                                        <b>${props.currentOrderTotal.toFixed(2)}</b>
                                    </span>
                                </li>}
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col d-flex flex-row-reverse" style={{ paddingTop: "0.5rem" }}>
                            {props.currentSandwich.ingredients.length > 0 &&
                                <button
                                    className="btn btn-secondary"
                                    onClick={() => props.placeOrder(props.currentSandwich)}>
                                    ORDER
                        </button>
                            }
                        </div>
                    </div>
                </React.Fragment>
            )}
        </div>
    );
}

export const ContainedOrder = connect(
    (reduxState: IReduxState) => {
        return {
            currentSandwich: reduxState.currentSandwich,
            currentOrderTotal: reduxState.currentOrderTotal
        }
    },
    (dispatch) => {
        return {
            placeOrder: (currentSandwich: Sandwich) => {
                dispatch(completeOrderAction(currentSandwich));
                alert("Order recieved!");
            }
        }
    }
)(Order);

