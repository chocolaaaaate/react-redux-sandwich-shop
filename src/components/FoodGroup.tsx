import React from 'react';
import { connect } from 'react-redux';
import IFoodItem from '../model/FoodItem';
import Sandwich from '../model/Sandwich';
import { addIngredientAction, removeIngredientAction } from '../redux/food';
import { IReduxState } from '../redux/shop';
import { FoodItem } from './FoodItem';


export class FoodGroup extends React.Component<{
    name: string,
    items: IFoodItem[],
    addIngredient: (i) => {},
    removeIngredient: (i) => {},
    currentSandwich: Sandwich
}> {

    checkboxEvent = (checked, item) => {
        console.log("checkbox event", "checked", checked, "itemname", item.name);

        if (checked) {
            this.props.addIngredient(item);
        } else {
            this.props.removeIngredient(item);
        }

    }

    isChecked = (item: IFoodItem) => {
        return (this.props.currentSandwich.ingredients?.length > 0) && (this.props.currentSandwich.ingredients.findIndex((ingr) => ingr.name === item.name) >= 0)
    }

    render() {
        return (
            <div style={{
                paddingBottom: "2rem"
            }}>
                <h5 style={{
                    color: "gray"
                }}>
                    {this.props.name}
                </h5>
                <div className="container">
                    <div className="row">
                        {this.props.items.map(it =>
                            <div key={it.name} className="col-md-3 col-sm-12 col-xs-12" >
                                <FoodItem
                                    foodCategoryName={this.props.name}
                                    item={it}
                                    isChecked={this.isChecked}
                                    onSelect={this.checkboxEvent} />
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export const ContainedFoodGroup = connect(
    (reduxState: IReduxState) => {
        return {
            currentSandwich: reduxState.currentSandwich
        }
    },
    (dispatch) => {
        return {
            addIngredient: (ingredient) => dispatch(addIngredientAction(ingredient)),
            removeIngredient: (ingredient) => dispatch(removeIngredientAction(ingredient))
        }
    }
)(FoodGroup);

