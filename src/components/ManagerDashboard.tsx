import * as React from 'react';
import { IReduxState } from "../redux/shop";
import { connect } from 'react-redux';

export const ManagerDashboard = (props) => {
    return (
        <div className="alert alert-secondary d-flex justify-content-center" role="alert">
            <div className="container">
                <div className="row" style={{ fontVariant: "petite-caps" }}>
                    <div className="col-4">
                        <span style={{ paddingRight: "2rem", fontWeight: "bold" }}>manager's dashboard</span>
                    </div>
                    <div className="col-4">
                        <span style={{ paddingRight: "2rem" }}>total orders: {props.totalOrderCount}</span>
                    </div>
                    <div className="col-4">
                        <span style={{ paddingRight: "2rem" }}>total sales: ${props.totalEarned.toFixed(2)}</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export const ContainedManagerDashboard = connect(
    (reduxState: IReduxState) => {
        const totalEarned = reduxState.completedOrders.length === 0 ? 0 : reduxState.completedOrders.map(o => o.total).reduce((prev, curr) => prev + curr);

        return {
            totalOrderCount: reduxState.completedOrders.length,
            totalEarned: totalEarned
        }
    }
)(ManagerDashboard);
