import React from 'react';
import '../css/FoodItem.css';

export const FoodItem = (props) => {
    const isSelected = props.isChecked(props.item);
    const isSoldOut = !isSelected && (props.item.quantity === 0);

    return (
        <React.Fragment>
            <label className="container">
                <div className={"row fooditem" + (isSelected ? " fooditem-selected" : "") + (isSoldOut ? " fooditem-soldout" : "")} >
                    <input className="col-12"
                        type="checkbox"
                        disabled={isSoldOut}
                        name={props.foodCategoryName}
                        value={props.item.name}
                        checked={isSelected}
                        onChange={(ev) => props.onSelect(ev.target.checked, props.item)}
                        style={{
                            display: "none"
                        }}
                    />
                    <p className="col-12 d-flex justify-content-center">
                        <b>{props.item.name}</b>
                    </p>
                    {isSoldOut ?
                        (
                            <p className="col-12 d-flex justify-content-center">
                                <span className="badge badge-danger">SOLD OUT</span>
                            </p>
                        ) :
                        (
                            <p className="col-12 d-flex justify-content-center">
                                <span className="badge badge-light">${props.item.cost.toFixed(2)}</span>
                            </p>
                        )
                    }
                </div>
            </label>
        </React.Fragment>
    );
}

