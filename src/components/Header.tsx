import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHamburger } from '@fortawesome/free-solid-svg-icons';
import { faBitbucket } from '@fortawesome/free-brands-svg-icons';

export const Header = (props) => {
    return (
        <nav className="navbar navbar-light bg-light">
            <span>
                {<FontAwesomeIcon icon={faHamburger} />}&nbsp;
                <span className="navbar-brand">Sumedh's Sandwich Shop</span>
            </span>
            <span style={{ fontVariant: "all-petite-caps" }}>
                Made using ReactJS &amp; Redux&nbsp;{<a href="https://bitbucket.org/chocolaaaaate/react-redux-sandwich-shop/" target="_blank"><FontAwesomeIcon color="#0357D1" icon={faBitbucket} /></a>}
            </span>
        </nav>
    );
}
